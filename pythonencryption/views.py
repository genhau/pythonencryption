# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.conf import settings
from django.views.generic.base import TemplateResponseMixin, View
from django.views.decorators.csrf import csrf_exempt
from Crypto.Cipher import AES
from Crypto import Random
import base64
import hashlib
import json

class AESCipher:
    def __init__( self, key ):
        self.bs = 32
        if len(key) >= 32:
            self.key = key[:32]
        else:
            self.key = self.pad(key)

    def encrypt( self, raw ):
        raw = self.pad(raw.encode('utf-8'))
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) ) 

    def decrypt( self, enc ):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return self.unpad(cipher.decrypt( enc[AES.block_size:] ))

    def pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    def unpad(self, s):
        return s[0:-ord(s[-1])]

class ServerView(TemplateResponseMixin, View):
    template_name = 'pythonencryption/server.html'

    def get(self, *args, **kwargs):
        #ctx = RequestContext()
        ctx =  kwargs
        return self.render_to_response(RequestContext(self.request, ctx))

    def post(self, *args, **kwargs):
        msg = self.request.POST["message"]
        cipher = AESCipher("This is a key123")
        return HttpResponse(cipher.encrypt(msg))

class Client1View(View):
    def post(self, *args, **kwargs):
        msg = self.request.POST["message"]
        cipher = AESCipher("This is a key123")
        return HttpResponse('[Car 1]: I\'m car 1, and your msg is ' + cipher.decrypt(msg))

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(Client1View, self).dispatch(*args, **kwargs)

class Client2View(View):
    def post(self, *args, **kwargs):
        msg = self.request.POST["message"]
        cipher = AESCipher("This is a key123")
        return HttpResponse('[Car 2]: I\'m car 2, and your msg is ' + cipher.decrypt(msg))

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(Client2View, self).dispatch(*args, **kwargs)

class Client3View(View):
    def post(self, *args, **kwargs):
        msg = self.request.POST["message"]
        cipher = AESCipher("This is a key123")
        return HttpResponse('[Car 3]: I\'m car 3, and your msg is ' + cipher.decrypt(msg))

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(Client3View, self).dispatch(*args, **kwargs)

Server = ServerView.as_view()
Client1 = Client1View.as_view()
Client2 = Client2View.as_view()
Client3 = Client3View.as_view()