from django.conf.urls import patterns, include, url

from django.contrib import admin
from . import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pythonencryption.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^server/', views.Server),
    url(r'^client1/', views.Client1),
    url(r'^client2/', views.Client2),
    url(r'^client3/', views.Client3),
)
